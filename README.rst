Bitlines
========

``bitlines`` is a tool for running bitbucket pipelines on your local machine. Two reasons you might want to do this:

1. To check your changes *before* you push them to the server and look silly
2. To allow you to create build artifacts inside the image (which you can then copy to your current directory with ``--extract``)

Note that ``bitlines`` uses the version of ``bitbucket-pipelines.yml`` in your local directory to test your most recent commit (i.e. uncommitted changes are ignored).

Command line
------------

.. code-block:: sh

  » bitlines --help
  Run bitbucket pipelines locally

  Usage:
  bitlines [<target>]
  bitlines -h|--help

  Arguments:
  <target>                   The name of the pipeline to run, e.g. default,
                              branches/default [default: default]

  Options:
  -p, --pipeline=<pipeline>  Path to a bitbucket pipelines yaml file [default:
                              bitbucket-pipelines.yml]
  -e, --env=<env>            Set environment variables
  -x, --extract=<extract>    Artifact to extract from build
  -v, --verbose              Verbose output
  -h, --help                 Show help message

Example
-------

Runing ``bitlines`` against itself

.. code-block:: sh

  » bitlines
  Pulling nimlang/nim:alpine
  alpine: Pulling from nimlang/nim
  Digest: sha256:fae341b4c6e1fa3c37a95bc546ce2fac6b202946cbc245a8104a58b03562f98b
  Status: Image is up to date for nimlang/nim:alpine
  docker.io/nimlang/nim:alpine
  + git clone /opt/pipelines/repo ./
  Cloning into '.'...
  done.
  + git config advice.detachedHead false
  + git checkout 03919205b31614121e440956ee5b4ba03b1eb12a
  HEAD is now at 0391920 Add mercurial and custom pipeline support
  + nimble refresh
  Downloading Official package list
      Success Package list downloaded.
  + nimble build
    Verifying dependencies for bitlines@0.1.0
  Installing yaml@>= 0.14.0
  Downloading https://github.com/flyx/NimYAML using git
    Verifying dependencies for yaml@0.14.0
  Installing yaml@0.14.0
    Success: yaml installed successfully.
  Installing therapist@> 0.1.0
  Downloading https://bitbucket.org/maxgrenderjones/therapist using git
    Verifying dependencies for therapist@0.2.0
  Installing therapist@0.2.0
    Building therapist/therapist using c backend
    Success: therapist installed successfully.
    Building bitlines/bitlines using c backend
  Pipeline succeeded
