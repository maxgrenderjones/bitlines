import therapist
import json
import os
import osproc
import posix
when (NimMajor, NimMinor, NimPatch) < (1, 6, 0):
    import posix_utils
else:
    import std/tempfiles
import strformat
import strutils
import yaml/tojson

const PIPELINES = "bitbucket-pipelines.yml"

type scmType = enum
    scmGit,
    scmHg

proc tryget(node: JsonNode, key: string, message: string): JsonNode =
    if key in node:
        result = node[key]
    else:
        quit(message, 1)

proc run(command: string, message: string) =
    let status = execShellCmd command
    if status!=0:
        quit(message, 1)

template withTempDir(prefix: string, code: untyped): untyped =
    # Attemps to use mkdtemp now fail on OSX, but createTempDir wasn't available before 1.6
    when (NimMajor, NimMinor, NimPatch) < (1, 6, 0):
        let tempdirname {.inject.} = absolutePath(mkdtemp(prefix))
        try:
            code
        finally:
            removeDir(tempdirname)
    else:
        let tempdirname {.inject.} = absolutePath(createTempDir(prefix, "tmp"))
        try:
            code
        finally:
            removeDir(tempdirname)

template withTempFile(prefix: string, suffix: string, code: untyped): untyped =
    when (NimMajor, NimMinor, NimPatch) < (1, 6, 0):
        let (filename, tempfile {.inject.}) = mkstemp(prefix, suffix)
        let tempfilename {.inject.} = absolutePath(filename)
        try:
            code
        finally:
            removeFile(tempfilename)
    else:
        let (tempfile {.inject.}, filename) = createTempFile(prefix, "tmp")
        let tempfilename {.inject.} = absolutePath(filename)
        try:
            code
        finally:
            removeFile(tempfilename)

proc mkstemps(tmpl: cstring, suffixlen: cint): cint {.importc, header: "<stdlib.h>", sideEffect.}
  ## Creates a unique temporary file.
  ##
  ## **Warning**: The `tmpl` argument is written to by `mkstemp` and thus
  ## can't be a string literal. If in doubt copy the string before passing it.

proc mkstemp(prefix: string, suffix: string): tuple[tempfilename: string, tempfile: File] =
    ## Creates a unique temporary file from a prefix string. Adds a six chars suffix.
    ## The file is created with perms 0600.
    ## Returns the filename and a file opened in r/w mode.
    var tmpl = cstring(prefix & "XXXXXX" & suffix)
    let fd = mkstemps(tmpl, cint(len(suffix)))
    var f: File
    if open(f, fd, fmReadWrite):
        return ($tmpl, f)
    raise newException(OSError, $strerror(errno))


proc run_pipeline(target: string, pipelines_filename: string, scm: scmType, envs = newSeq[string](), extracts = newSeq[string](), verbose=false) =
    if not fileExists(pipelines_filename):
        quit(fmt"{pipelines_filename} not found")
    let pipelines = readFile(pipelines_filename).loadToJson()[0]
    let file_image = pipelines.tryget("image", "Pipeline has no image")
    let file_image_name = file_image.tryget("name", "Image does not specify an image name").getStr()
    let (revision, scmstatus) = case scm
        of scmGit:
            execCmdEx("git rev-parse HEAD")
        of scmHg:
            execCmdEx("hg log -r . --template '{node}'")
    if scmstatus!=0:
        quit(revision, scmstatus)

    let checkout = case scm
        of scmGit:
            @[
                "git clone /opt/pipelines/repo ./",
                "git config advice.detachedHead false",
                fmt"git checkout {revision}"
            ]
        of scmHg:
            @[
                # Provide a fallback in case pip isn't installed
                "pip install --upgrade mercurial || hg --version",
                "hg clone /opt/pipelines/repo ./",
                fmt"hg update {revision}",
            ]
    let teardown = @[
        ""
    ]
    
    let pipeline = pipelines.tryget("pipelines", fmt"{PIPELINES} file is missing a 'pipelines' section")
    var maybe_parallel_steps = pipeline
    for fragment in target.split('/'):
        maybe_parallel_steps = maybe_parallel_steps.tryget(fragment, fmt"{target} not found")
    let steps = if "parallel" in maybe_parallel_steps[0]: maybe_parallel_steps[0]["parallel"] else: maybe_parallel_steps
    for step in steps:
        let step = step.tryget("step", fmt"Expected 'step' in {step.pretty}")
        let image_name = if "image" in step: step["image"].getStr else: file_image_name
        let script = step.tryget("script", "Expected 'script' in {step.pretty}")
        
        var commands = newSeq[string]()
        for line in script:
            commands.add(line.getStr)

        echo fmt"Pulling {image_name}"
        run(fmt"docker pull {image_name}", "Could not pull docker image")
        var status: int
        let (whichbash, bashstatus) = execCmdEx(fmt"docker run --rm --entrypoint /bin/sh {image_name} -c 'which bash'")
        let (shlink, shstatus) = execCmdEx(fmt"docker run --rm --entrypoint /bin/sh {image_name} -c 'readlink -f /bin/sh'")
        let setup = if bashstatus==0:
            @[
                fmt"#!{whichbash.splitLines[0]}",
                "set -euxo pipefail"
            ]
        elif shstatus==0 and extractFilename(shlink.splitLines[0]) in ["busybox"]:
            @[
                "#!/bin/sh",
                "set -euxo pipefail"
            ]
        else:
            quit(fmt"Unsupported shell: {shlink}")

        withTempDir("pipelines"):
            withTempFile("pipeline", ".sh"):
                for line in (setup & checkout & commands & teardown):
                    if verbose:
                        echo line
                    tempfile.writeLine(line)
                tempfile.flushFile()
                discard chmod(tempfilename, Mode(S_IRUSR + S_IWUSR + S_IXUSR))
                let cwd = os.getCurrentDir()
                let docker_command = fmt"docker run --rm -v '{tempfilename}':/pipeline.sh -v '{cwd}':/opt/pipelines/repo -v '{tempdirname}':/opt/pipelines/app -w /opt/pipelines/app --entrypoint /bin/sh {image_name} -c /pipeline.sh"
                if verbose:
                    echo fmt"> {docker_command}"
                status = execShellCmd(docker_command)
                if status==0:
                    for extract in extracts:
                        let src = tempdirname / extract
                        if fileExists(src):
                            let dest = extractFilename(extract)
                            copyFileWithPermissions(src, dest)
                            echo fmt"{extract}->{dest}"
                        else:
                            echo fmt"{extract} not found"

        # Check status outside the with block to allow the temporary files to be cleaned up
        if status==0:        
            echo "Pipeline succeeded"
        else:
            quit("Pipeline failed", 1)

if isMainModule:
    let spec = (
        target: newStringArg(@["<target>"], defaultVal="default", optional=true, help="The name of the pipeline to run, e.g. default, branches/default"),
        pipeline: newStringArg(@["-p", "--pipeline"], defaultVal=PIPELINES, help="Path to a bitbucket pipelines yaml file"),
        env: newStringArg(@["-e", "--env"], multi=true, help="Set environment variables"),
        extract: newStringArg(@["-x", "--extract"], multi=true, help="Artifact to extract from build"),
        verbose: newCountArg("-v, --verbose", help="Verbose output"),
        help: newHelpArg()
    )
    spec.parseOrQuit(
        prolog="Run bitbucket pipelines locally"
    )
    let scm = if dirExists(".hg"):
        scmHg
    elif dirExists(".git"):
        scmGit
    else:
        quit("Could not find source code management directory")
    run_pipeline(spec.target.value, spec.pipeline.value, scm, spec.env.values, spec.extract.values, verbose=spec.verbose.seen)
