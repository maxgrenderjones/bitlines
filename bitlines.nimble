# Package

version       = "0.1.0"
author        = "Max Grender-Jones"
description   = "Run bitbucket pipelines locally"
license       = "MIT"
srcDir        = "src"
bin           = @["bitlines"]

# Dependencies

requires "nim >= 1.0.0"
requires "yaml >= 0.14.0"
requires "therapist > 0.1.0"

import strformat, os

task docs, "Builds documentation":
    let builddir = "build" / "docs"
    mkDir builddir
    selfExec fmt"rst2html --hints:off --outdir:{builddir} README.rst"

task pipelines, "Builds bitlines for multiple targets":
    when defined(osx):
        exec "nim c -o:bitlines.osx src/bitlines.nim"
        exec "bitlines custom/build -x bitlines.amd64 -x bitlines.arm64 -x bitlines.armhf"